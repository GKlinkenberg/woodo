Woodo
=====

This program outputs "This is a weird tree." if run as a user. When using root/superuser it will output ascii-art of the pokemon sudowoodo.

Installation
============

1. Use make to create the executable, if you changed the source or don't trust the executable in this repo.
2. Copy the woodo file to /usr/bin (or any other folder that is in your path), or just run the install.sh file as root.
