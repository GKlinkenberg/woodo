CC=gcc
SOURCES=woodo.c
CFLAGS=-pedantic -Wall

all:
	$(CC) $(CFLAGS) $(SOURCES) -o woodo
