#include <stdio.h>
#include <unistd.h>

int main(int argc, char* argv[]){
	if( getuid() != 0 ){
		printf("This is a weird tree.\n");
	} else {
        printf("     _              __\n");
        printf("    / `\\  (~._    ./  )\n");
        printf("    \\__/ __`-_\\__/ ./\n");
        printf("   _ \\ \\/  \\   \\ |_    __\n");
        printf(" (   )  \\__/ -^    \\  /  \\\n");
        printf("  \\_/ \"  \\  | o  o  | .. / __\n");
        printf("       \\\\. -' ====  /  || /  \\ \n");
        printf("         \\   .  .  |---__.\\__/\n");
        printf("         /  :     /   |   |\n");
        printf("         /   :   /     \\_/\n");
        printf("      --/ ::    (\n");
        printf("     (  |     (  (____\n");
        printf("   .--  .. ----**.____)\n");
        printf("   \\___/          \n");
    }
    
    return 0;
}
